/*
 * Pluto, a test automation thing
 * Copyright (C) 2014 Matthew Flint <m@tthew.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tthew.pluto.automation;

import org.tthew.pluto.automation.finders.IFindAllButtonsCriteria;
import org.tthew.pluto.automation.finders.IFindAllFramesCriteria;
import org.tthew.pluto.automation.finders.IFindSingleButtonCriteria;
import org.tthew.pluto.automation.finders.IFindSingleFrameCriteria;
import org.tthew.pluto.automation.finders.impl.CriteriaImplFactory;

public class Destination
{
	private static abstract class ADestination implements IAutomation
	{
		public IFindSingleFrameCriteria frame()
		{
			return new CriteriaImplFactory(this)
					.buildCriteria(IFindSingleFrameCriteria.class);
		}

		public IFindAllFramesCriteria frames()
		{
			return new CriteriaImplFactory(this)
					.buildCriteria(IFindAllFramesCriteria.class);
		}

		public IFindSingleButtonCriteria button()
		{
			final IFindSingleButtonCriteria buttonCriteria = new CriteriaImplFactory(
					this).buildCriteria(IFindSingleButtonCriteria.class);

			// default "inside" criteria" - inside any frame
			buttonCriteria.inside(this.frames());

			return buttonCriteria;
		}

		public IFindAllButtonsCriteria buttons()
		{
			final IFindAllButtonsCriteria buttonCriteria = new CriteriaImplFactory(
					this).buildCriteria(IFindAllButtonsCriteria.class);

			// default "inside" criteria" - inside any frame
			buttonCriteria.inside(this.frames());

			return buttonCriteria;
		}
	}

	private static final class LocalJavaAutomationImpl extends ADestination
	{

	}

	private Destination()
	{

	}

	public static IAutomation localJavaAutomation()
	{
		return new LocalJavaAutomationImpl();
	}
}
