/*
 * Pluto, a test automation thing
 * Copyright (C) 2014 Matthew Flint <m@tthew.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tthew.pluto.automation;

import org.tthew.pluto.automation.components.IClickable;
import org.tthew.pluto.automation.components.ICloseable;
import org.tthew.pluto.automation.components.IComponent;
import org.tthew.pluto.automation.components.IEnableable;
import org.tthew.pluto.automation.finders.IFindAll;
import org.tthew.pluto.automation.finders.IFindOne;

public final class Actions
{
	public static final void click(final IFindOne<? extends IClickable> criteria)
	{
		// criteria.find().click();
	}

	public static final void checkThereAre(final int expected,
			final IFindAll<? extends IComponent> criteria)
	{
		// criteria.checkThereAre(expected);
	}

	public static final void close(final IFindOne<? extends ICloseable> criteria)
	{
		// criteria.find().close();
	}

	public static final void closeAll(
			final IFindAll<? extends ICloseable> criteria)
	{
		// for (final ICloseable component : criteria.findAll())
		// {
		// component.close();
		// }
	}

	public static final void assertEnabled(
			final IFindOne<? extends IEnableable> criteria)
	{
		// criteria.find().assertEnabled();
	}

	public static final void assertEnabled(
			final IFindAll<? extends IEnableable> criteria)
	{
		// for (final IEnableable component : criteria.findAll())
		// {
		// component.assertEnabled();
		// }
	}
}
