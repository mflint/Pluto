/*
 * Pluto, a test automation thing
 * Copyright (C) 2014 Matthew Flint <m@tthew.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.tthew.pluto.example;

import static org.tthew.pluto.automation.Actions.assertEnabled;
import static org.tthew.pluto.automation.Actions.checkThereAre;
import static org.tthew.pluto.automation.Actions.click;
import static org.tthew.pluto.automation.Actions.close;
import static org.tthew.pluto.automation.Actions.closeAll;

import org.tthew.pluto.automation.Destination;
import org.tthew.pluto.automation.IAutomation;
import org.tthew.pluto.automation.components.IButton;

public final class Example
{
	public void example()
	{
		final IAutomation application = Destination.localJavaAutomation();

		application.buttons().withLabel("OK").count();
		application.button().withLabel("OK").click();
		application.buttons().withLabel("OK").checkThereAre(2);

		// check the number of buttons
		application.buttons()
				.inside(application.frame().withTitle("Properties"))
				.checkThereAre(6);

		// alternative form
		checkThereAre(
				6,
				application.buttons().inside(
						application.frame().withTitle("Properties")));

		// click first found button in first found frame
		application.button().withLabel("Cancel")
				.inside(application.frame().withTitle("Properties")).click();

		// alternative form
		click(application.button().withLabel("Cancel")
				.inside(application.frame().withTitle("Properties")));

		final IButton[] b = application.buttons().withLabel("OK").findAll();
		b[0].click();

		for (final IButton button : application.buttons().withLabel("Click me"))
		{
			button.click();
		}

		// close the frame with a given title
		application.frame().withTitle("Lovely frame").close();

		// alternate forms
		close(application.frame().withTitle("Lovely frame"));
		closeAll(application.frames());

		// close the frames containing a certain button
		closeAll(application.frames().containing(
				application.button().withLabel("Wiggle")));

		// checking that buttons are enabled
		application.button().withLabel("OK").assertEnabled();
		assertEnabled(application.button().withLabel("OK"));
		assertEnabled(application.buttons().inside(application.frames()));
		assertEnabled(application.buttons().inside(
				application.frame().withTitle("Lovely frame")));
	}
}
