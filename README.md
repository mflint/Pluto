Pluto
=====

Pluto is a non-functional brain-dump of ideas for a UI unit testing framework

Aims
----

Pluto would aim to be:
* readable
* flexible
* carefree
* useful

But will probably be:
* perpetually unfinished
* generics hell
* abandoned if things get tricky

Example code
------------

Where ``application`` is some magical object which has a handle on a running application somewhere:

```
IFrame[] frames = application.frames();
```

```
IFrame[] frames = application.frames().withTitle("My lovely application");
```

```
click(application.button().withLabel("Cancel")
    .inside(application.frame().withTitle("Properties")));
```

```
closeAll(application.frames());
```

```
closeAll(application.frames().containing(
    application.button().withLabel("Wiggle")));
```

```
assertEnabled(application.button().withLabel("OK"));
```

```
checkThereAre(6,
	application.buttons().inside(
			application.frame().withTitle("Properties")));
```
